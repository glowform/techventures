<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class=" cf">

					<div id="main" class="m-all cf" role="main">

						<article id="post-not-found" class="hentry cf">

							<header class="article-header">

								<div class="wrap">

									<h1><?php _e( 'Epic 404 - Article Not Found', 'corisetheme' ); ?></h1>

								</div>

							</header>

							<section class="entry-content">

								<div class="wrap">

									<p><?php _e( 'The article you were looking for was not found, but maybe try looking again!', 'corisetheme' ); ?></p>

								</div>

							</section>

						</article>

					</div>

				</div>

			</div>

<?php get_footer(); ?>
