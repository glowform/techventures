<?php
/*
 Template Name: Contact Page Template
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>

			<div id="content" class="contact-page">

				<div id="inner-content" class="cf">

						<div id="main" class="m-all cf" role="main">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

								<header class="article-header">

									<div class="wrap">

										<h1 class="page-title" itemprop="headline"><?php 

										if (get_field('page_title') != '') {
											echo get_field('page_title');
										} else {
											the_title(); 
										}

										?></h1>

									</div>

								</header> <?php // end article header ?>

								<section class="entry-content cf" itemprop="articleBody">
									<div class="wrap cf">
										<div class="m-all t-1of2 d-1of2">
											
											<?php echo do_shortcode('[contact-form-7 id="14" title="Contact form 1"]'); ?>
										</div>

										<div class="m-all t-1of2 d-1of2 last">
											<?php
												// the content (pretty self explanatory huh)
												the_content();
											?>
										</div>
									</div>
								</section>


							</article>

							<?php endwhile; else : ?>

									<article id="post-not-found" class="hentry cf">
											<header class="article-header">
												<h1><?php _e( 'Oops, Post Not Found!', 'corisetheme' ); ?></h1>
										</header>
											<section class="entry-content">
												<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'corisetheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the page-custom.php template.', 'corisetheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>

						</div>

				</div>

			</div>


<?php get_footer(); ?>
