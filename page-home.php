<?php
/*
 Template Name: Home Page Template
 
*/
?>

<?php get_header(); ?>

			<div id="content">

				

				<div id="inner-content" class="cf">

						<div id="main" class="m-all" role="main">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

								<section class="home-top">
									<?php echo get_field('top_text') ?>
								</section>

								<section class="bg-gray">
									<div class="wrap cf">
										<?php echo get_field('top_gray_box') ?>
									</div>
								</section>

								<section class="home-middle">
									<div class="wrap cf reorder">
										<div class="m-all t-1of3 d-1of3 order-1">
											<div class="home-box">
												<img src="<?php echo get_template_directory_uri(); ?>/library/images/couple-man-woman.svg" alt="">
												<?php echo get_field('middle_left_1') ?>
											</div>

											<div class="home-box">
												<img src="<?php echo get_template_directory_uri(); ?>/library/images/money-wallet-open.svg" alt="">
												<?php echo get_field('middle_left_2') ?>
											</div>
											
										</div>

										<div class="m-all t-1of3 d-1of3 order-3">
											<div class="home-box cf home-img">
												<?php echo get_field('middle_image'); ?>
											</div>
											
										</div>

										<div class="m-all t-1of3 d-1of3 order-2">
											<div class="home-box">
												<img src="<?php echo get_template_directory_uri(); ?>/library/images/design-tool-pen-station.svg" alt="">
												<?php echo get_field('middle_right_1') ?>
											</div>

											<div class="home-box">
												<img src="<?php echo get_template_directory_uri(); ?>/library/images/screen-1.svg" alt="">
												<?php echo get_field('middle_right_2') ?>
											</div>
											
										</div>
									</div>
								</section>

								<section class="bg-gray">
									<div class="wrap cf">
										<?php echo get_field('middle_gray_box') ?>
									</div>
								</section>


								<section class="home-bottom">
									<div class="wrap cf">
										<div class="cf reorder">
											<div class="m-all t-1of3 d-1of3 order-2">
												<div class="home-box">
													<?php echo get_field('bottom_left_1') ?>
												</div>

											</div>

											<div class="m-all t-1of3 d-1of3">
												<p></p>
												
											</div>

											<div class="m-all t-1of3 d-1of3 order-1">
												<div class="home-box">
													<img src="<?php echo get_template_directory_uri(); ?>/library/images/mood-happy.svg" alt="">
												</div>
												
											</div>
										</div>
										<div class="cf">
											<div class="m-all t-1of3 d-1of3">
												<div class="home-box">
													<div class="home-box">
														<img src="<?php echo get_template_directory_uri(); ?>/library/images/mouse.svg" alt="">
													</div>
													
												</div>

											</div>

											<div class="m-all t-1of3 d-1of3">
												<p></p>
												
											</div>

											<div class="m-all t-1of3 d-1of3">
												<?php echo get_field('bottom_right_1') ?>
												
											</div>
										</div>
										<div class="cf reorder">
											<div class="m-all t-1of3 d-1of3 order-2">
												<div class="home-box">
													<?php echo get_field('bottom_left_2') ?>
												</div>

											</div>

											<div class="m-all t-1of3 d-1of3">
												<p></p>
												
											</div>

											<div class="m-all t-1of3 d-1of3 order-1">
												<div class="home-box">
													<img src="<?php echo get_template_directory_uri(); ?>/library/images/send-email-2.svg" alt="">
												</div>
												
											</div>
										</div>
									</div>
								</section>

								<section class="bg-gray">
									<div class="wrap cf">
										<?php echo get_field('bottom_gray_box') ?>
									</div>
								</section>




							</article>

							<?php endwhile; else : ?>

									<article id="post-not-found" class="hentry cf">
											<header class="article-header">
												<h1><?php _e( 'Oops, Post Not Found!', 'corisetheme' ); ?></h1>
										</header>
											<section class="entry-content">
												<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'corisetheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the page-custom.php template.', 'corisetheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>

						</div>

				</div>

			</div>


<?php get_footer(); ?>
