<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class=" cf">

						<div id="main" class="m-all cf" role="main">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

								<header class="article-header">

									<div class="wrap">

										<h1 class="page-title" itemprop="headline"><?php 

										if (get_field('page_title') != '') {
											echo get_field('page_title');
										} else {
											the_title(); 
										}

										?></h1>

									</div>

								</header> <?php // end article header ?>

								<section class="entry-content cf" itemprop="articleBody">

									<div class="wrap">
										<?php
											// the content (pretty self explanatory huh)
											the_content();

										?>
									</div>
								</section> <?php // end article section ?>


							</article>

							<?php endwhile; else : ?>

									<article id="post-not-found" class="hentry cf">
										<div class="wrap">
											<header class="article-header">
												<h1><?php _e( 'Oops, Post Not Found!', 'corisetheme' ); ?></h1>
											</header>
											<section class="entry-content">
												<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'corisetheme' ); ?></p>
											</section>
											<footer class="article-footer">
													<p><?php _e( 'This is the error message in the page.php template.', 'corisetheme' ); ?></p>
											</footer>
										</div>
									</article>

							<?php endif; ?>

						</div>

				</div>

			</div>

<?php get_footer(); ?>
